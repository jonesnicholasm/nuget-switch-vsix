# Visual Studio - Nuget Package Switcher Extension

The goal of this repository is to create a simple visual studio extension that will be able to switch between (owned) project source code and nuget
libraries in order to aid debugging and code referencing within the ide (targeting Visual Studio 2019).


## Installation

The end goal will be to have a .vsix file that will install the extension into Visual Studio 2019.

## Usage

TODO: write usage with screenshots

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)